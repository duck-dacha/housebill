#!/usr/bin/env python3

"""Script for interacting with Splitwise for housebills"""

# System libs
import datetime
import email
import email.policy
import logging
import os
import re
import sys

# pip libraries
import yaml
from splitwise import Splitwise
from splitwise.expense import Expense

LOGGER = logging.getLogger(__name__)

def login(config, swise):
    """Login to splitwise, using saved token or fetching a new one"""
    access_token = config['creds'].get('access_token')
    if not access_token:
        url, secret = swise.getAuthorizeURL()
        print(f"Please visit {url}")
        oauth_token = input("Token: ")
        oauth_verifier = input("Verifier: ")
        access_token = swise.getAccessToken(oauth_token, secret, oauth_verifier)
        print(f"New access token: {access_token}")
    swise.setAccessToken(access_token)


def dump_debts(swise, group):
    """Display debts in this group"""
    LOGGER.debug(group.__dict__)
    for debt in group.simplified_debts:
        LOGGER.debug(debt.__dict__)
        LOGGER.debug(swise.getUser(debt.fromUser).__dict__)
        LOGGER.debug(swise.getUser(debt.toUser).__dict__)


def build_category_map(cats, by_name):
    """Recursively add categories to name->category map"""
    for category in cats:
        by_name[category.name] = category
        build_category_map(category.subcategories, by_name)

def fetch_categories(swise):
    """Fetch categories from Splitwise"""
    categories = swise.getCategories()
    by_name = {}
    build_category_map(categories, by_name)
    return by_name

EVERSOURCE_PLAIN = """Bill date: BILL_DATE Due date: DUE_DATE Total Amount Due: $AMOUNT  Account Number: ******ACCT_NUM1 Dear Eversource Customer, Your Eversource monthly  bill is now available to view and pay online.  Your account is enrolled in Auto Pay.""" # pylint:disable=line-too-long

def parse_eversource_email():
    """Parse Eversource bill email and extract account number and amount"""
    bill_re = re.escape(EVERSOURCE_PLAIN)
    bill_re = bill_re.replace("BILL_DATE", r"(?P<bill_date>[0-9/]{10})")
    bill_re = bill_re.replace("DUE_DATE", r"(?P<due_date>[0-9/]{10})")
    bill_re = bill_re.replace("ACCT_NUM1", r"(?P<acct>[0-9]{4})")
    bill_re = bill_re.replace("AMOUNT", r"(?P<amount>[0-9.]+)")
    msg = email.message_from_file(sys.stdin, policy=email.policy.default)
    mime = msg.get_body(('plain', ))
    body = mime.get_content()
    LOGGER.debug(mime)
    LOGGER.debug(body)
    match = re.search(bill_re, body)
    LOGGER.info("match=%s", match)
    due_date = datetime.datetime.strptime(match.group("due_date"), "%m/%d/%Y").date()
    bill_date = datetime.datetime.strptime(match.group("bill_date"), "%m/%d/%Y").date()
    return match.group("acct"), match.group("amount"), due_date, bill_date

def add_utility_payment(swise, group, accounts, categories):
    """Add Eversource payment to Splitwise based on email"""
    try:
        acct, amount, due_date, bill_date = parse_eversource_email()
    except AttributeError:
        LOGGER.error("Couldn't parse bill -- AttributeError")
        raise
    except KeyError:
        LOGGER.error("Couldn't parse bill -- KeyError")
        raise
    category = accounts[acct]
    desc = f"Eversource: {category} ({acct}) [auto]"
    now = datetime.datetime.now()
    details = f"Bill dated {bill_date}, due {due_date}. Auto-imported at {now}."

    expense = Expense()
    expense.setCost(amount)
    expense.setDescription(desc)
    expense.setDetails(details)
    expense.setCategory(categories[category])
    expense.setDate(due_date.isoformat())
    expense.setGroupId(group.getId())
    expense.setSplitEqually(True)
    new_expense, errors = swise.createExpense(expense)
    LOGGER.debug("expense=%s errors=%s", new_expense.__dict__, errors)


def run(operation):
    """Run operation"""
    with open('config.yml', 'r', encoding='utf8') as config_fp:
        config = yaml.safe_load(config_fp)
    swise = Splitwise(config['creds']['key'], config['creds']['secret'])
    login(config, swise)
    #groups = swise.getGroups()
    group = swise.getGroup(config['group'])
    categories = fetch_categories(swise)
    if operation == 'add_util':
        add_utility_payment(swise, group, config['accounts']['eversource'], categories)
    elif operation == 'dump':
        dump_debts(swise, group)
    elif operation == 'remind':
        pass
    else:
        raise NotImplementedError(f"Unexpected operation {operation}")


def setup_logging():
    """Set up logging configuration"""
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logging.getLogger().setLevel(logging.INFO)
    LOGGER.setLevel(logging.DEBUG)


    # File handler
    fh = logging.FileHandler(filename='housebill.log', encoding='utf-8')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logging.getLogger().addHandler(fh)

    # Console handler
    if 'HOUSEBILL_NOCONSOLE' in os.environ:
        LOGGER.info("Skipping console logging because HOUSEBILL_NOCONSOLE set")
    else:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logging.getLogger().addHandler(ch)


if __name__ == '__main__':
    setup_logging()
    run(sys.argv[1])
