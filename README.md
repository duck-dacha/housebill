Utilities for managing housebills with Splitwise -- adding utility bills to
Splitwise and sending reminder emails to residents.

## Setup

Copy `sample-config.yml` to `config.yml` and appropriately fill in the various
secrets, group ID, and account numbers.

The various credentials can be acquired by going to
https://secure.splitwise.com/apps and choosing to "Register your application"
(probably, I haven't set this up in years and didn't take notes at the time).

The group ID can be found by going to https://secure.splitwise.com/#/dashboard,
choosing the group in the left panel, and looking at the URL (something like
https://secure.splitwise.com/#/groups/123456789 -- use the number at the end).

The four digit account number can be found in the emails Eversource is sending,
or on their website.
