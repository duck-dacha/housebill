#/bin/sh

set -euf

cd `dirname "$0"`
env HOUSEBILL_NOCONSOLE= venv/bin/python housebill.py add_util
