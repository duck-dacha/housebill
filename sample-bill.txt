EN-US New  Bill from Eversource
    
                
 EBILL
 
 Account Number: 
******1234 
 Bill date: 
 01/23/2021 
 Due date: 
 02/13/2021 
 Total Amount Due: 
 $123.45 
 [1] 
 
 Account Number: 
******1234
 Dear Eversource Customer,
 Your Eversource monthly bill is now available to view and pay online[2].

 Your account is enrolled in Auto Pay. The total amount due of $123.45 has been scheduled. You will receive an email when your payment has been processed.

Log on to Eversource.com[3]. While online, make sure to check for important Customer Communications[4] and Billing Rights[5].

 If you are having trouble paying your bill, we have flexible payment plans and financial assistance programs[6] to help.

 
 Sincerely,
 Eversource Customer Service.

 
 This email was sent to EMAIL for Eversource account ending with 1234.
Log in to update your email address, to view, unsubscribe or update your alert preferences. 
This is an automated message. Please do not reply. 
To manage your notification preferences click here[7] 
 [8] [9]

Links:
  [1] https://www.eversource.com/
  [2] https://www.eversource.com/
  [3] https://eversource.com
  [4] https://www.eversource.com/content/ema-c/residential/my-account/billing-payments/about-your-bill/monthly-customer-communications
  [5] https://www.eversource.com/content/ema-c/residential/my-account/billing-payments/about-your-bill/bill-rights
  [6] https://www.eversource.com/content/ema-c/residential/my-account/billing-payments/help-pay-my-bill
  [7] https://eversource.com/Content
  [8] http://www.facebook.com
  [9] http://www.twitter.com
